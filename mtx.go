package main

import "sync/atomic"

type InstantMutex uint32

func (m *InstantMutex) TryLock() bool {
	return atomic.CompareAndSwapUint32((*uint32)(m), 0, 1)
}

func (m *InstantMutex) Unlock() {
	if !atomic.CompareAndSwapUint32((*uint32)(m), 1, 0) {
		panic("unlock of locked mutex")
	}
}
