package main

import (
	"bytes"
	"fmt"
	"log"
	"net/http"
	"strings"
)

//go:generate go run gen/genAssets/genAssets.go
//go:generate go run gen/genApiRegisterHandlers/genApiRegisterHandlers.go

func (s *server) setupWeb() {
	http.HandleFunc("/", s.handleIndex())
	http.HandleFunc("/assets/", s.handleAssets())
	http.HandleFunc("/api/", s.handleAPIroot())

	s.api_reg()

	log.Println("server up")
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", s.listenport), nil))
}

func (s *server) handleIndex() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			http.NotFound(w, r)
			log.Println("failed index path:", r.URL.Path)
			return
		}
		w.Header().Add("Content-Type", asset_mime["assets/index.html"])
		bytes.NewBuffer(asset_map["assets/index.html"]).WriteTo(w)
	}
}

func (s *server) handleAssets() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		asset_key := strings.TrimLeft(r.URL.Path, "/")
		v, ok := asset_map[asset_key]
		if !ok {
			http.NotFound(w, r)
			log.Println("failed path:", r.URL.Path)
			return
		}
		w.Header().Add("Content-Type", asset_mime[asset_key])
		bytes.NewBuffer(v).WriteTo(w)
	}
}

func (s *server) handleAPIroot() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		http.NotFound(w, r)
		log.Println("failed path:", r.URL.Path)
	}
}
