module gitlab.com/rigel314/pi-timelapse

go 1.13

require (
	github.com/dhowden/raspicam v0.0.0-20190323051945-60ef25a6629f
	github.com/dustin/go-humanize v1.0.0
	github.com/gorilla/schema v1.1.0
	github.com/luoluoluo/resize v0.0.0-20190524050039-0e2c769b8a5f
	github.com/tednix/mjpeg v0.0.0-20191016121854-fde38f3c1b45
)
