// +build !linux

package main

import "fmt"

func stat() (used, total uint64, err error) {
	return 0, 0, fmt.Errorf("not supported on this platform")
}
