package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"unsafe"

	"github.com/dustin/go-humanize"
	"github.com/gorilla/schema"
)

type api_response struct {
	Success bool
	Reason  string
}

func (s *server) recording_list() http.HandlerFunc {
	type recording struct {
		Text string
	}
	type response struct {
		api_response
		Recordings []recording
	}

	return func(w http.ResponseWriter, r *http.Request) {
		j := json.NewEncoder(w)

		f, err := ioutil.ReadDir(s.recordingPath)
		if err != nil {
			log.Println(err)
			j.Encode(response{api_response: api_response{Success: false}})
			return
		}
		rec := make([]recording, 0, len(f))
		for _, v := range f {
			if v.IsDir() {
				rec = append(rec, recording{Text: v.Name()})
			}
		}

		j.Encode(response{
			api_response: api_response{Success: true},
			Recordings:   rec,
		})
	}
}

func (s *server) control_status() http.HandlerFunc {
	type response struct {
		api_response
		Recording     bool
		RecordingName string
	}

	return func(w http.ResponseWriter, r *http.Request) {
		recording := s.recordState.Load().(bool)
		name := ""
		if recording {
			name = s.recordName.Load().(string)
		}
		j := json.NewEncoder(w)
		j.Encode(response{
			api_response:  api_response{Success: true},
			Recording:     recording,
			RecordingName: name,
		})
	}
}

func (s *server) system_monitor() http.HandlerFunc {
	type response struct {
		api_response
		DiskText string
	}

	return func(w http.ResponseWriter, r *http.Request) {
		u := s.diskUsage.Load().(dUsage)
		j := json.NewEncoder(w)
		j.Encode(response{
			api_response: api_response{Success: true},
			DiskText:     fmt.Sprintf("%s / %s (%.2f%% used)", humanize.Bytes(u.used), humanize.Bytes(u.total), float64(u.used)/float64(u.total)*100),
		})
	}
}

func (s *server) live_image() http.HandlerFunc {
	type request struct {
		Have uint64
	}
	type response struct {
		api_response
		Handle uintptr
		Image  string
	}

	return func(w http.ResponseWriter, r *http.Request) {
		sd := schema.NewDecoder()
		var req request
		err := sd.Decode(&req, r.URL.Query())
		if err != nil {
			log.Println(err)
		}

		j := json.NewEncoder(w)
		b := s.lastImage.Load().([]byte)

		var handle uintptr
		if len(b) > 0 {
			handle = uintptr(unsafe.Pointer(&b[0]))
		}
		var img string
		if handle != uintptr(req.Have) {
			img = base64.StdEncoding.EncodeToString(b)
		}
		j.Encode(response{
			api_response: api_response{Success: true},
			Handle:       handle,
			Image:        img,
		})
	}
}

func (s *server) start_stop() http.HandlerFunc {
	type request struct {
		Kind          string
		RecordingName string
	}
	type response struct {
		api_response
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var ret bool = false

		sd := schema.NewDecoder()
		var req request
		err := sd.Decode(&req, r.URL.Query())
		if err != nil {
			log.Println(err)
		}

		switch req.Kind {
		case "stop":
			if s.recordState.Load().(bool) == false {
				break
			}
			s.recordState.Store(false)
			ret = true
		case "start":
			if s.recordState.Load().(bool) == true {
				break
			}
			if req.RecordingName == "" {
				break
			}
			reqpath := filepath.Join(s.recordingPath, req.RecordingName)
			if !s.ValidPath(reqpath) {
				break
			}
			err := os.Mkdir(reqpath, os.ModeDir|0775)
			if err != nil {
				break
			}
			s.recordName.Store(req.RecordingName)
			s.recordState.Store(true)
			ret = true
		default:
		}

		je := json.NewEncoder(w)
		je.Encode(response{
			api_response: api_response{Success: ret},
		})
	}
}

func (s *server) cancel_create_video() http.HandlerFunc {
	type response struct {
		api_response
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var ret bool

		// close cancel channel
		s.createVideoCancelMtx.Lock()
		select {
		default:
			ret = true
			close(s.createVideoCancel)
		case <-s.createVideoCancel:
		}
		s.createVideoCancelMtx.Unlock()

		je := json.NewEncoder(w)
		je.Encode(response{
			api_response: api_response{Success: ret},
		})
	}
}

func (s *server) create_video_progress() http.HandlerFunc {
	type response struct {
		api_response
		createVidProgress
	}

	return func(w http.ResponseWriter, r *http.Request) {
		p := s.createVideoProgress.Load().(createVidProgress)
		je := json.NewEncoder(w)
		je.Encode(response{
			api_response:      api_response{Success: true},
			createVidProgress: p,
		})
	}
}

type video_request struct {
	RecordingName string
	Force         bool
	Width, Height uint32
	FPS           uint32
}

var defaultVideoRequest = video_request{
	Width: 800, Height: 600,
	FPS: 10,
}

func (s *server) video_exists() http.HandlerFunc {
	type response struct {
		api_response
		Exists bool
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var reason string
		var ret, exist bool

		defer func() {
			je := json.NewEncoder(w)
			je.Encode(response{
				api_response: api_response{Success: ret, Reason: reason},
				Exists:       exist,
			})
		}()

		sd := schema.NewDecoder()
		var req = defaultVideoRequest
		err := sd.Decode(&req, r.URL.Query())
		if err != nil {
			log.Println(err)
		}

		reqpath := filepath.Join(s.recordingPath, req.RecordingName)
		if !s.ValidPath(reqpath) {
			reason = "invalid path: " + reqpath
			return
		}

		recording := s.recordState.Load().(bool)
		name := s.recordName.Load().(string)
		if recording && req.RecordingName == name {
			reason = "can't download the same video as current recording"
			return
		}

		creating := s.createVideoProgress.Load().(createVidProgress)
		if creating.RecordingName == req.RecordingName {
			reason = "can't download the same video as current creation"
			return
		}

		ret = true
		fname := filepath.Join(s.recordingPath, req.RecordingName, fmt.Sprintf("video_%05dx%05d_%03dfps.avi", req.Width, req.Height, req.FPS))
		final, err := os.Open(fname)
		if err != nil {
			reason = "doesn't exist"
			return
		}
		final.Close()

		exist = true
	}
}

func (s *server) create_video() http.HandlerFunc {
	type response struct {
		api_response
	}

	return func(w http.ResponseWriter, r *http.Request) {
		var reason string
		var ret bool

		sd := schema.NewDecoder()
		var req = defaultVideoRequest
		err := sd.Decode(&req, r.URL.Query())
		if err != nil {
			log.Println(err)
		}

		defer func() {
			je := json.NewEncoder(w)
			je.Encode(response{
				api_response: api_response{Success: ret, Reason: reason},
			})
		}()

		if !s.createVideoMtx.TryLock() {
			reason = "couldn't lock"
			return
		}

		s.createVideoCancel = make(chan struct{})
		go s.genVid(req)
		ret = true
	}
}

func (s *server) download_video() http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var ret bool

		sd := schema.NewDecoder()
		var req = defaultVideoRequest
		err := sd.Decode(&req, r.URL.Query())
		if err != nil {
			log.Println(err)
		}

		defer func() {
			if !ret {
				http.NotFound(w, r)
			}
		}()

		reqpath := filepath.Join(s.recordingPath, req.RecordingName)
		if !s.ValidPath(reqpath) {
			return
		}

		recording := s.recordState.Load().(bool)
		name := s.recordName.Load().(string)
		if recording && req.RecordingName == name {
			return
		}

		creating := s.createVideoProgress.Load().(createVidProgress)
		if creating.RecordingName == req.RecordingName {
			return
		}

		fname := filepath.Join(s.recordingPath, req.RecordingName, fmt.Sprintf("video_%05dx%05d_%03dfps.avi", req.Width, req.Height, req.FPS))
		final, err := os.Open(fname)
		if err != nil {
			return
		}
		// log.Println("begin mjpeg transfer:", reqpath)
		w.Header().Add("Content-Type", "video/x-msvideo")
		w.Header().Add("Content-Disposition", `attachment; filename="`+req.RecordingName+`.avi"`)
		io.Copy(w, final)
		final.Close()
		// log.Println("mjpeg transfer complete:", reqpath)

		ret = true
	}
}

func (s *server) delete_recording() http.HandlerFunc {
	type request struct {
		RecordingName string
	}
	type response struct {
		api_response
	}

	return func(w http.ResponseWriter, r *http.Request) {
		http.NotFound(w, r)
	}
}
