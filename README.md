picam-timelapse
======

# About
Simple webapp to run on a pi to take timelapse videos

# Dependencies
* raspistill

# Building
`GOARM=7 GOOS=linux GOARCH=arm go build -ldflags "-buildid= -w -s" -trimpath .`
