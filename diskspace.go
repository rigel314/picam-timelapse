// +build linux

package main

import (
	"syscall"
)

func stat() (used, total uint64, err error) {
	var s syscall.Statfs_t

	err = syscall.Statfs("/", &s)

	return (s.Blocks - s.Bavail) * uint64(s.Bsize), s.Blocks * uint64(s.Bsize), err
}
