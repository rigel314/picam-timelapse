let refresh_time = 1000

let recording_name = ""
let recording = false

let live_image_handle = 0

// on page load
$(function () {
    $("#start").click(function () {
        $.get("/api/start_stop", { Kind: "start", RecordingName: $("#recording_name").val() }, function (data) {
            if (!data.Success) {
                console.log("start_stop(start) reported failure")
                return
            }
            $.when(control_status(), recording_list())
        }, "json").fail(function () {
            console.log("start_stop(start) get failed")
        })
    })
    $("#stop").click(function () {
        $.get("/api/start_stop", { Kind: "stop" }, function (data) {
            if (!data.Success) {
                console.log("start_stop(stop) reported failure")
                return
            }
            $.when(control_status())
        }, "json").fail(function () {
            console.log("start_stop(stop) get failed")
        })
    })
    $("#delete").click(function () {
        let rec = $("#recording_selector").val()
        $.get("/api/delete_recording", { RecordingName: rec }, function (data) {
            if (!data.Success) {
                console.log("delete(" + rec + ") reported failure")
                return
            }
            // TODO: handle response
        }, "json").fail(function () {
            console.log("delete(" + rec + ") get failed")
        })
    })
    $("#cancel").click(function () {
        let rec = $("#recording_selector").val()
        $.get("/api/cancel_create_video", { RecordingName: rec }, function (data) {
            if (!data.Success) {
                console.log("cancel_create_video(" + rec + ") reported failure")
                return
            }
            $.when(create_video_progress())
        }, "json").fail(function () {
            console.log("cancel_create_video(" + rec + ") get failed")
        })
    })
    $("#recording_selector").change(function () {
        let rec = $("#recording_selector").val()
        let disable = (rec == recording_name)
        $("#delete")[0].disabled = disable
        $("#download")[0].disabled = disable
        $.when(video_exists())
    })

    recording_list()

    refreshAPI()
})

function refreshAPI() {
    $.when(
        control_status(),
        create_video_progress(),
        video_exists(),
        $.get("/api/system_monitor", "", function (data) {
            if (!data.Success) {
                console.log("system_monitor reported failure")
                return
            }
            $("#disk_space").text(data.DiskText)
        }, "json").fail(function () {
            console.log("system_monitor get failed")
        }),
        $.get("/api/live_image", { Have: live_image_handle }, function (data) {
            if (!data.Success) {
                console.log("live_image reported failure")
                return
            }
            if (live_image_handle != data.Handle) {
                $("#preview-img")[0].src = "data:image/jpeg;base64," + data.Image
            }
            live_image_handle = data.Handle
        }, "json").fail(function () {
            console.log("live_image get failed")
        })
    ).always(function () {
        setTimeout(refreshAPI, refresh_time)
    })
}

function control_status() {
    return $.get("/api/control_status", "", function (data) {
        if (!data.Success) {
            console.log("control_status reported failure")
            return
        }
        $("#start")[0].disabled = data.Recording
        $("#stop")[0].disabled = !data.Recording
        $("#recording_name")[0].disabled = data.Recording
        if (data.Recording) {
            $("#recording_name").val(data.RecordingName)
        }
        recording = data.Recording
        recording_name = data.RecordingName
    }, "json").fail(function () {
        console.log("control_status get failed")
    })
}

function recording_list() {
    return $.get("/api/recording_list", "", function (data) {
        if (!data.Success) {
            console.log("recording_list reported failure")
            return
        }
        $("#recording_selector").empty()
        $("#recording_selector").append($("<option></option>").text("--- please choose ---"))
        for (let index = 0; index < data.Recordings.length; index++) {
            const element = data.Recordings[index];
            $("#recording_selector").append($("<option></option>").text(element.Text))
        }
    }, "json").fail(function () {
        console.log("recording_list get failed")
    })
}

function create_video_progress() {
    return $.get("/api/create_video_progress", "", function (data) {
        if (!data.Success) {
            console.log("create_video_progress reported failure")
            return
        }
        let status = ""
        if (data.InProgress) {
            status = "Processing " + data.RecordingName + ": " + Math.round(data.Index / data.Total * 10000) / 100 + "%"
        } else if (data.CreateVidFailed) {
            status = "failed"
        } else {
            status = "idle"
        }
        $("#recording_action_status").text(status)
    }, "json").fail(function () {
        console.log("create_video_progress get failed")
    })
}

function video_exists() {
    let rec = $("#recording_selector").val()
    if (rec == "--- please choose ---") {
        return $.when()
    }
    return $.when(
        $.get("/api/video_exists", { RecordingName: rec }, function (data) {
            if (!data.Success) {
                // console.log("video_exists(" + rec + ") reported failure")
                return
            }
            let downloadText = ""
            let downloadFunc
            if (data.Exists) {
                downloadText = "Download"
                downloadFunc = function (e) {
                    e.preventDefault();  // stop the browser from following
                    window.location.href = "/api/download_video?RecordingName=" + rec;
                }
            } else {
                downloadText = "Start Video Creation"
                downloadFunc = function (e) {
                    let rec = $("#recording_selector").val()
                    $.get("/api/create_video", { RecordingName: rec }, function (data) {
                        if (!data.Success) {
                            console.log("create(" + rec + ") reported failure")
                            return
                        }
                        $.when(create_video_progress())
                    }, "json").fail(function () {
                        console.log("create(" + rec + ") get failed")
                    })
                }
            }
            $("#download").text(downloadText)
            $("#download").off("click")
            $("#download").click(downloadFunc)
        }, "json").fail(function () {
            console.log("video_exists(" + rec + ") get failed")
        }),
        $.get("/api/video_exists", { RecordingName: rec, Width: 2592, Height: 1944 }, function (data) {
            if (!data.Success) {
                // console.log("video_exists(" + rec + ") reported failure")
                return
            }
            let downloadText = ""
            let downloadFuncRaw
            if (data.Exists) {
                downloadText = "Download (Raw)"
                downloadFuncRaw = function (e) {
                    e.preventDefault();  // stop the browser from following
                    window.location.href = "/api/download_video?RecordingName=" + rec + "&Width=2592&Height=1944";
                }
            } else {
                downloadText = "Start Video Creation (Raw)"
                downloadFuncRaw = function (e) {
                    let rec = $("#recording_selector").val()
                    $.get("/api/create_video", { RecordingName: rec, Width: 2592, Height: 1944 }, function (data) {
                        if (!data.Success) {
                            console.log("create(" + rec + ") reported failure")
                            return
                        }
                        $.when(create_video_progress())
                    }, "json").fail(function () {
                        console.log("create(" + rec + ") get failed")
                    })
                }
            }
            $("#download-raw").text(downloadText)
            $("#download-raw").off("click")
            $("#download-raw").click(downloadFuncRaw)

        }, "json").fail(function () {
            console.log("video_exists(" + rec + ") get failed")
        }))
}
