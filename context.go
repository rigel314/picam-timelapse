package main

import (
	"fmt"
	"time"
)

func (s *server) Deadline() (deadline time.Time, ok bool) {
	return time.Time{}, false
}

func (s *server) Done() <-chan struct{} {
	return s.ctx.Done()
}

func (s *server) Err() error {
	select {
	case <-s.ctx.Done():
		return fmt.Errorf("channel closed")
	default:
		return nil
	}
}

func (s *server) Value(key interface{}) interface{} {
	return nil
}
