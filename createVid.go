package main

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"

	"github.com/luoluoluo/resize"
	"github.com/tednix/mjpeg"
)

func (s *server) genVid(req video_request) {
	var reason string
	var ret bool

	defer func() {
		s.createVideoMtx.Unlock()

		// close cancel channel
		s.createVideoCancelMtx.Lock()
		select {
		default:
			close(s.createVideoCancel)
		case <-s.createVideoCancel:
		}
		s.createVideoCancelMtx.Unlock()

		s.createVideoProgress.Store(createVidProgress{InProgress: false, Index: 0, Total: 1, CreateVidFailed: !ret, FailureReason: reason})
	}()

	reqpath := filepath.Join(s.recordingPath, req.RecordingName)
	if !s.ValidPath(reqpath) {
		reason = "invalid path: " + reqpath
		return
	}

	s.createVideoProgress.Store(createVidProgress{InProgress: true, Index: 0, Total: 1, RecordingName: req.RecordingName})

	recording := s.recordState.Load().(bool)
	name := s.recordName.Load().(string)
	if recording && req.RecordingName == name {
		reason = "can't download the same video as current recording"
		return
	}

	fname := filepath.Join(reqpath, fmt.Sprintf("video_%05dx%05d_%03dfps.avi", req.Width, req.Height, req.FPS))
	if info, err := os.Stat(fname); err == nil && !info.IsDir() && !req.Force {
		// already exists, assume we don't have to recreate the mjpeg
		reason = "already exists"
		ret = true
		return
	}

	files, err := ioutil.ReadDir(reqpath)
	if err != nil {
		reason = "readdir err: " + err.Error()
		return
	}

	var trueWidth uint32
	var trueHeight uint32
	for _, file := range files {
		if file.IsDir() {
			continue
		}
		n := filepath.Join(reqpath, file.Name())
		if !strings.HasSuffix(n, ".jpg") {
			continue
		}
		file, err := os.Open(n)
		if err != nil {
			reason = "open failed: " + err.Error()
			return
		}
		img, err := jpeg.Decode(file)
		if err != nil {
			reason = "jpeg decode failed: " + err.Error()
			return
		}
		trueWidth = uint32(img.Bounds().Max.X)
		trueHeight = uint32(img.Bounds().Max.Y)
		break
	}

	if trueWidth == 0 && trueHeight == 0 {
		reason = "No images in directory"
		return
	}

	// log.Println("begin mjpeg create:", reqpath)
	w, h := req.Width, req.Height
	if w == 0 && h == 0 {
		w, h = trueWidth, trueHeight
	}
	aw, err := mjpeg.New(fname, int32(w), int32(h), 10)
	if err != nil {
		reason = "mjpeg err: " + err.Error()
		return
	}

	data := &bytes.Buffer{}
	for i, file := range files {
		s.createVideoProgress.Store(createVidProgress{InProgress: true, Index: uint32(i), Total: uint32(len(files)), RecordingName: req.RecordingName})
		// log.Printf("%d/%d: %s\n", i, len(files), filepath.Join(reqpath, file.Name()))
		if file.IsDir() {
			continue
		}
		if !strings.HasSuffix(file.Name(), ".jpg") {
			continue
		}
		select {
		case <-s.createVideoCancel:
			aw.Close()
			os.Remove(fname)
			reason = "request canceled"
			ret = true
			return
		default:
		}
		img, err := resize.Open(filepath.Join(reqpath, file.Name()))
		if err != nil {
			aw.Close()
			os.Remove(fname)
			reason = "readfile err: " + err.Error()
			return
		}
		var m image.Image
		if w != trueWidth || h != trueHeight {
			m = resize.Resize(uint(w), uint(h), img, resize.NearestNeighbor)
		} else {
			m = img
		}
		data.Reset()
		err = jpeg.Encode(data, m, nil)
		if err != nil {
			aw.Close()
			os.Remove(fname)
			reason = "encode err: " + err.Error()
			return
		}
		err = aw.AddFrame(data.Bytes())
		if err != nil {
			aw.Close()
			os.Remove(fname)
			reason = "addframe err: " + err.Error()
			return
		}
	}

	err = aw.Close()
	if err != nil {
		os.Remove(fname)
		reason = "mjpeg close err: " + err.Error()
		return
	}

	ret = true
}
