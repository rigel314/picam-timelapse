package main

import (
	"bytes"
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/dhowden/raspicam"
)

type server struct {
	listenport           int
	recordingPath        string
	recordState          atomic.Value
	recordName           atomic.Value
	diskUsage            atomic.Value
	lastImage            atomic.Value
	createVideoProgress  atomic.Value
	createVideoMtx       InstantMutex
	createVideoCancel    chan struct{}
	createVideoCancelMtx sync.Mutex
	ctx                  context.Context
}

func main() {
	var s server

	flag.IntVar(&s.listenport, "port", 80, "http port to listen on")
	flag.StringVar(&s.recordingPath, "recordingPath", "/opt/picam-timelapse", "basePath for recordings")
	flag.Parse()

	var err error
	s.recordingPath, err = filepath.Abs(s.recordingPath)
	if err != nil {
		log.Fatal(err)
	}

	s.initValues()

	var cancel context.CancelFunc
	s.ctx, cancel = context.WithCancel(context.Background())

	go s.setupWeb()
	go s.imageLoop()
	go s.monitorLoop()

	<-s.ctx.Done()

	cancel()
}

type createVidProgress struct {
	InProgress      bool
	Index, Total    uint32
	CreateVidFailed bool
	FailureReason   string
	RecordingName   string
}

func (s *server) initValues() {
	s.diskUsage.Store(dUsage{used: 1, total: 1})
	s.recordState.Store(false)
	s.recordName.Store("")
	s.lastImage.Store([]byte{})
	s.createVideoProgress.Store(createVidProgress{InProgress: false, Index: 0, Total: 1})
	s.createVideoCancel = make(chan struct{})
	close(s.createVideoCancel)
}

func (s *server) imageLoop() {
	cam := raspicam.NewStill()
	cam.Timeout = 300 * time.Millisecond
	// cam.Width = 800
	// cam.Height = 600

	var count uint64
	t := time.NewTimer(time.Second)
	for {
		buf := &bytes.Buffer{}
		errCh := make(chan error)
		go func() {
			for v := range errCh {
				log.Println(v)
			}
		}()
		raspicam.Capture(cam, buf, errCh)
		s.lastImage.Store(buf.Bytes())
		if s.recordState.Load().(bool) {
			if count%10 == 0 {
				// save images to current recording folder
				imgname := fmt.Sprintf("%023d.jpg", time.Now().UnixNano())
				f, err := os.Create(filepath.Join(s.recordingPath, s.recordName.Load().(string), imgname))
				if err != nil {
					log.Println(err)
				} else {
					_, err := buf.WriteTo(f)
					if err != nil {
						log.Println(err)
					}
					f.Close()
				}
			}
			count++
		} else {
			count = 0
		}
		<-t.C
		t.Reset(time.Second)
	}
}

type dUsage struct {
	used, total uint64
}

func (s *server) monitorLoop() {
	for {
		used, total, err := stat()
		if err == nil {
			s.diskUsage.Store(dUsage{used: used, total: total})
		}
		time.Sleep(time.Second)
	}
}

func Abs(in string) string {
	out, _ := filepath.Abs(in)
	return out
}

func (s *server) ValidPath(in string) bool {
	in, err := filepath.Abs(in)
	if err != nil {
		return false
	}
	return strings.HasPrefix(in, s.recordingPath)
}
