package main

import (
	"net/http"
)

func (s* server) api_reg() {
	http.HandleFunc("/api/recording_list", s.recording_list())
	http.HandleFunc("/api/control_status", s.control_status())
	http.HandleFunc("/api/system_monitor", s.system_monitor())
	http.HandleFunc("/api/live_image", s.live_image())
	http.HandleFunc("/api/start_stop", s.start_stop())
	http.HandleFunc("/api/cancel_create_video", s.cancel_create_video())
	http.HandleFunc("/api/create_video_progress", s.create_video_progress())
	http.HandleFunc("/api/video_exists", s.video_exists())
	http.HandleFunc("/api/create_video", s.create_video())
	http.HandleFunc("/api/download_video", s.download_video())
	http.HandleFunc("/api/delete_recording", s.delete_recording())
}
