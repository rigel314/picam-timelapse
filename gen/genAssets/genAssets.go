package main

import (
	"io/ioutil"
	"mime"
	"os"
	"path"
	"path/filepath"
	"strings"
	"text/template"
)

var asset_mime_override = map[string]string{
	"assets/index.js":            "application/javascript",
	"assets/jquery-3.4.1.min.js": "application/javascript",
}

func main() {
	f, err := os.Create("http_assets.go")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	t, err := template.New("http_assets.tmpl").Funcs(map[string]interface{}{
		"nameSlug": nameSlug,
	}).ParseFiles("gen/genAssets/http_assets.tmpl")
	if err != nil {
		panic(err)
	}

	var elems []elem
	filepath.Walk("assets", func(fpath string, info os.FileInfo, err error) error {
		if err != nil {
			panic(err)
		}
		if info.IsDir() {
			return nil
		}
		f, err := os.Open(fpath)
		if err != nil {
			panic(err)
		}
		b, err := ioutil.ReadAll(f)
		if err != nil {
			panic(err)
		}

		elems = append(elems, elem{
			Name: info.Name(),
			Data: b,
			MIME: mime.TypeByExtension(path.Ext(fpath)),
		})
		f.Close()
		return nil
	})

	// override any mime types that might not go generate correctly
	for _, e := range elems {
		if m, ok := asset_mime_override[e.Name]; ok {
			e.MIME = m
		}
	}

	err = t.Execute(f, elems)
	if err != nil {
		panic(err)
	}
}

func nameSlug(fname string) string {
	return strings.Map(func(r rune) rune {
		if (r >= 'a' && r <= 'z') ||
			(r >= 'A' && r <= 'Z') ||
			(r >= '0' && r <= '9') {
			return r
		} else {
			return '_'
		}
	}, fname)
}

type elem struct {
	Name string
	Data []byte
	MIME string
}
