package main

import (
	"go/ast"
	"go/parser"
	"go/token"
	"os"
	"text/template"
)

func main() {
	f, err := os.Create("api_gen.go")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	t, err := template.New("api_gen.tmpl").ParseFiles("gen/genApiRegisterHandlers/api_gen.tmpl")
	if err != nil {
		panic(err)
	}

	fset := token.NewFileSet()
	astf, err := parser.ParseFile(fset, "api.go", nil, 0)
	if err != nil {
		panic(err)
	}

	var funcs []string

	for _, d := range astf.Decls {
		if n, ok := getFuncName(d); ok {
			funcs = append(funcs, n)
		}
	}

	err = t.Execute(f, funcs)
	if err != nil {
		panic(err)
	}
}

func getFuncName(d ast.Decl) (string, bool) {
	if f, ok := d.(*ast.FuncDecl); ok {
		return f.Name.Name, true
	}
	return "", false
}
